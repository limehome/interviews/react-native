## React Native - Coding Challenge

Our new native react app is an important part of our strategy to make our limehome guest experience even better. 
We want to create a premium experience for our guests by developing not only a mobile app but also an online concierge, a front desk and everything they would need during their stay to be usable right on their phone - **so we created this challenge to test our candidates' overall developer skills.**


### Requirements

For this challenge you're going to create a basic limehome app, this will give us an idea about your coding skills.

Your app should list the available limehome apartment in Berlin on a MapView, to access this list use the following URL:

- [GET /properties](https://api.limehome.com/properties/v1/public/properties/?cityId=32&adults=1)
- [GET /properties/210](https://api.limehome.com/properties/v1/public/properties/210)

We expect most people to spend between **4 and 6 hours** on this challenge, although you're free to use more time if you want.
So keep it simple, but also remember to show off your skills. We want to see what you can do.
We expect you to submit a link to your code(e.g. using Github, Gitlab, etc.).
We should be able to quicly run it via `Expo CLI`

#### What we check:

- Functionality
- UI look and feel (minimal but appealing)
- Code quality (e.g. code style and tests)
- Extra points (not required):
     - 	Animations


### Suggested design and assets:

You can find bellow a design mockup for the challenge. Feel free to use it or create your own: [UI/UX Mocks](https://www.figma.com/proto/JVquz0r58pvrsEe5fP6pHM/Case-Study?page-id=0%3A1&node-id=0%3A1913&viewport=241%2C48%2C0.74&scaling=min-zoom&starting-point-node-id=0%3A1913).

For the assets we recommend the material icons: [https://material.io](https://material.io/resources/icons/?style=baseline)

--

If you have any questions or feedback about the challenge, don't hesitate to reach out to us: [tech-hiring@limehome.com](tech-hiring@limehome.com)
Good luck with the challenge! We are looking forward to your solution!
